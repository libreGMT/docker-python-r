# Use an official Python runtime as a parent image
FROM python:latest

# Install R
RUN apt-get update -y && \
    apt-get install -y gcc r-base r-base-dev

# Install R packages
RUN R -e "install.packages('tidyverse', dependencies=TRUE, repos='https://pbil.univ-lyon1.fr/CRAN/')"

CMD ["/bin/bash"]